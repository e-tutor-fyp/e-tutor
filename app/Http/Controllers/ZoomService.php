<?php

namespace App\Service;

use App\Model\ExpertDetail;
use App\Model\User;
use Illuminate\Support\Facades\Http;
use App\Relation\StartupExpert;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class ZoomService
{
    //
    private function generateZoomToken($id)
    {
        $expert = User::find($id);
        if(Carbon::parse($expert->details->token_expiry) < Carbon::now()){
            self::refreshToken(['refresh_token' => $expert->details->zoom_refresh_token, 'id' => $expert->id]);
            $expert = User::find($id);
        }
        return $expert->details->zoom_access_token;
        
    }
    private function retrieveZoomUrl()
    {
        return config('services.zoom.url');
    }

    private function zoomRequest($id)
    {
        $jwt = $this->generateZoomToken($id);
        return \Illuminate\Support\Facades\Http::withHeaders([
            'authorization' => 'Bearer ' . $jwt,
            'content-type' => 'application/json',
        ]);
    }

    public function zoomMeeting(string $path, array $body = [])
    {
        $appointment = StartupExpert::find($ ['id']);

        $sendRequest = [
            'topic' => 'Appointment with '.$appointment->expert->details->name,
            'type' => 2,
			'start_time' => ZoomService::toZoomTimeFormat(Carbon::parse($appointment->start_time),),
			'timezone' => 'UTC',
            'duration' => Carbon::parse($appointment->start_time)->diffInMinutes(Carbon::parse($appointment->end_time)),
			'agenda' => 'Your appointment with '.$appointment->expert->details->name.' is set for the requested services.',
			'settings' => [
				'host_video' => false,
				'participant_video' => false,
				'waiting_room' => true,
			]
        ];
        $url = $this->retrieveZoomUrl();
        $request = $this->zoomRequest($appointment->expert_id);
        return $request->post($url . $path, $sendRequest);
    }

    public static function toZoomTimeFormat(string $dateTime)
	{
			$date = new \DateTime($dateTime);
			return $date->format('Y-m-d\TH:i:s');
    }
    
    public function zoomAuth($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Basic '.base64_encode(config('services.zoom.client_id').':'.config('services.zoom.client_secret'))
        ])->post(config('services.zoom.oauth_url').'token?grant_type=authorization_code&code='.$data['code'].'&redirect_uri='.config('services.zoom.redirect_url'), [
        ]);
        ExpertDetail::addZoomTokens([
			'access_token' => $response['access_token'],
            'refresh_token' => $response['refresh_token'],
			'id' => $data['id'],
        ]);
        $zoomId = $this->getZoomId($data['id']);
        ExpertDetail::addZoomId([
            'zoom_user_id' => $zoomId['id'],
            'id' => $data['id']
            ]);
    }

    public function zoomAuthWP($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Basic '.base64_encode(config('services.zoom.client_id').':'.config('services.zoom.client_secret'))
        ])->post(config('services.zoom.oauth_url').'token?grant_type=authorization_code&code='.$data['code'].'&redirect_uri=https%3A%2F%2Fwpcal.io%2Fcal-api-receive-it%2F&client_id=wUhJIYpcQ0aT7eqUiH4I9Q', [
        ]);
        ExpertDetail::addZoomTokens([
			'access_token' => $response['access_token'],
            'refresh_token' => $response['refresh_token'],
			'id' => $data['id'],
        ]);
        $zoomId = $this->getZoomId($data['id']);
        ExpertDetail::addZoomId([
            'zoom_user_id' => $zoomId['id'],
            'id' => $data['id'],
            'zoom_user_name' => $zoomId['first_name'].' '.$zoomId['last_name'],
            'zoom_user_email' => $zoomId['email'],
            ]);
        return 'success';
    }

    public static function refreshToken($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Basic '.base64_encode(config('services.zoom.client_id').':'.config('services.zoom.client_secret'))
        ])->post(config('services.zoom.oauth_url').'token?grant_type=refresh_token&refresh_token='.$data['refresh_token'], [
        ]);
        ExpertDetail::addZoomTokens([
			'access_token' => $response['access_token'],
			'refresh_token' => $response['refresh_token'],
			'id' => $data['id']
		]);
    }

    public function getZoomId($data)
    {
        $request = $this->zoomRequest($data);
        $url = config('services.zoom.url');
        return $request->get($url . 'users/me');
    }

    public static function returnOauthUrl($data)
    {
		$state = Crypt::encrypt($data['id']);
        return config('services.zoom.oauth_url').'authorize?response_type=code&client_id='.config('services.zoom.client_id').'&redirect_uri='.config('services.zoom.redirect_url').'&state='.$state;
    }

    public static function returnOauthUrl($data)
    {
        // $state = Crypt::encrypt($data['id']);
        $url = config('services.zoom.redirect_url').'/';
         $step1 = base64_encode('{"site_redirect_url":"'.$url.'?wpcal_action=tp_account_receive_token&provider=zoom_meeting&state='.$data['id'].'","user_id":"'.$data['id'].'"}');
         $step2 = base64_encode('https://zoom.us/oauth/authorize?state='.$step1.'&scope=read&response_type=code&approval_prompt=auto&redirect_uri=https%3A%2F%2Fwpcal.io%2Fcal-api-receive-it%2F&client_id=wUhJIYpcQ0aT7eqUiH4I9Q');
        return "https://wpcal.io/cal-api/?tp_provider=zoom_meeting&passed_data=".$step2;
    }
}
