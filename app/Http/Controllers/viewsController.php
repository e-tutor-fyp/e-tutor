<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\CourseStudent;
use App\Models\CourseTeacher;
use Auth;
class viewsController extends Controller
{
    public function index()
    {
        return view('home');
    }
    public function about()
    {
        return view('about');
    }
    public function contact()
    {
        return view('contact');
    }
    public function course()
    {
        $course = Courses::all();
        return view('course')->with(compact('course'));
    }
    public function course_single($id)
    {
        $course = Courses::find($id);
        return view('course-single')->with(compact('course'));
    }
    public function teachers()
    {
        return view('teachers');
    }
    public function teacher_single()
    {
        return view('teacher-single');
    }
    public function notice()
    {
        return view('notice');
    }
    public function notice_single()
    {
        return view('notice-single');
    }
    public function research()
    {
        return view('research');
    }
    public function scolarship()
    {
        return view('scolarship');
    }

    //panel
    //student
    public function studentDashboard($id)
    {

        $data = [];
        $course = CourseStudent::where('user_id', $id)->get();
        foreach ($course as $key => $value) {
            $data[$key] = Courses::find($value->course_id);
        }
        return view('Panel.student-dashboard')->with(compact('data'));
    }
    public function paymentDetails()
    {
        return view('Panel.PaymentDetails');
    }
    public function profile()
    {
        return view('Panel.profile');
    }
    //teacher
    public function teacherdashboard($id)
    {
        $data = [];
        $course = CourseTeacher::where('user_id', $id)->get();
        foreach ($course as $key => $value) {
            $data[$key] = Courses::find($value->course_id);
        }
        return view('Panel.teacher-dashboard')->with(compact('data'));
    }
    public function bdetail()
    {
        return view('Panel.bank-details');
    }
    public function tprofile()
    {
        return view('Panel.tprofile');
    }
    //admin
    public function adashboard()
    {
        return view('Panel.admin-dashboard');
    }
    public function acourse()
    {
        return view('Panel.addcourse');
    }
}
