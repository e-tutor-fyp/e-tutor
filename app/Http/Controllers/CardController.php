<?php

namespace App\Http\Controllers;

use App\Models\Cards;
use App\Models\PaymentHistory;
use App\Models\User;
use Illuminate\Http\Request;
use Stripe;

class CardController extends Controller
{
    public function addCard(Request $request)
    {
        $user = User::findOrFail($requUser
        $card = Stripe::cards()->create($user->stripe_customer_id, $token['id']);
        $db_card = Cards::create([
            "user_id" => $user->id,
            "stripe_token" => $card['id'],
            'expire_date' => $card['exp_month'] . ' / ' . $card['exp_year'],
            "last_four_digit" => $card['last4'],
            "name" => $card['brand'],
        ]);
        return 'success';
    }
    public function showUserAllCards(Request $request)
    {
        $user = User::find($request->user_id);
        $cards = Cards::where([['user_id', $user->id])->get();
        return view('Panel.PaymentDetails')->with(compact('cards'));
    }
    public function Charge(Request $request)
    {
        $user = User::find($request->user_id);
        $card = Cards::find($request->card_id);

        Stripe::customers()->update($user->customer_stripe_id, [
            'default_source' => $card->card_id,
        ]);
        $charge = Stripe::charges()->create([
            'customer' => $user->customer_stripe_id,
            'currency' => 'PKR',
            'amount' => $request->total_amount,
            "description" => $request->description,
        ]);
        $PaymentHistory = new PaymentHistory();
        $PaymentHistory->user_id = $user->id;
        $PaymentHistory->card_id = $card->id;
        $PaymentHistory->amount = $request->total_amount;
        $PaymentHistory->description = $charge['description'];
        $PaymentHistory->transaction_id = $charge['id'];
        $PaymentHistory->save();

        return 'success';

    }
}
