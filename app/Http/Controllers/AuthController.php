<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\CourseStudent;
use App\Models\CourseTeacher;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Stripe;
use Validator;

class AuthController extends Controller
{
    public function Register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users,email',
                'password' => 'required|min:6',
                'role' => 'required|min:5',
            ]);
            if ($validator->fails()) {
                return ([
                    'message' => $validator->errors(),
                    'code' => '',
                ]);
            }
            $customer = Stripe::customers()->create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'description' => $request->description,
                'address[line1]' => $request->address,
                'address[city]' => $request->city,
                'address[country]' => $request->country_code,
                'address[state]' => $request->state,
            ]);
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => $request->role,
                'contact' => $request->contact,
                'city' => $request->city,
                'country' => $request->country,
                'address' => $request->address,
                'stripe_customer_id' => $customer['id'],
            ]);

            $message = "User registered successfully";
            if ($request->role == 'student') {
                return view('Panel.student-dashboard');
            } elseif ($request->role == 'teacher') {
                return view('Panel.teacher-dashboard');
            } else {
                return view('Panel.admin-dashboard');
            }
        } catch (
            \Exception |
            \Cartalyst\Stripe\Exception\MissingParameterException |
            \Cartalyst\Stripe\Exception\BadRequestException |
            \Cartalyst\Stripe\Exception\InvalidRequestException |
            \Cartalyst\Stripe\Exception\NotFoundException |
            \Cartalyst\Stripe\Exception\CardErrorException |
            \Cartalyst\Stripe\Exception\ServerErrorException $e
        ) {
            return ([
                'message' => $e->getMessage(),
                'code' => '',
            ]);
        }
    }
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'Email' => 'required|email|max:255',
            'Password' => 'required|min:6',
            'role' => 'required|min:5',
        ]);
        if ($validator->fails()) {
            return ([
                'message' => $validator->errors(),
                'code' => '',
            ]);
        }
        $credentials = [];
        $credentials['email'] = $request->Email;
        $credentials['password'] = $request->Password;
        $credentials['role'] = $request->role;
        $token = $request->_token;
        try {
            $user = User::where('email', $request->Email)->where('role', $request->role)->first();
            $user->makeHidden(['password']);

            $message = "User Login successfully";
            $data = array(
                'user' => $user,
            );
            if ($request->role == 'student') {
                $data = [];
                $course = CourseStudent::where('user_id', $user->id)->get();
                if ($course) {
                    foreach ($course as $key => $value) {
                        $data[$key]['course'] = Courses::find($value->course_id);
                        $data[$key]['zoom'] = $value->zoom_link;
                    }
                }
                return view('Panel.student-dashboard')->with(compact('data'));
            } elseif ($request->role == 'teacher') {
                $data = [];
                $course = CourseTeacher::where('user_id', $user->id)->get();
                foreach ($course as $key => $value) {
                    $data[$key] = Courses::find($value->course_id);
                }
                return view('Panel.teacher-dashboard')->with(compact('data'));
            } else {
                return view('Panel.admin-dashboard');
            }
        } catch (\Exception $e) {
            return ([
                'message' => $e->getMessage(),
                'code' => '',
            ]);
        }
    }
    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return redirect()->route('home');
    }
}
