<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\CourseStudent;
use App\Models\CourseTeacher;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function createCourse(Request $request)
    {
        $course = Courses::create($request);
        return response([
            'code' => 200,
            'msg' => 'Success',
            'data' => $course,
        ]);
    }
    public function deleteCourse($id)
    {
        $course = Courses::findOrFail($id);
        $course->delete();
        return response([
            'code' => 200,
            'msg' => 'Success',
        ]);
    }
    public function updateCourse($id, Requset $request)
    {
        $course = Courses::findOrFail($id);
        $course->update($request);
        return response([
            'code' => 200,
            'msg' => 'Success',
            'data' => $course,
        ]);
    }
    public function studentCourse(Request $request)
    {
        CourseStudent::create($request);
        return response([
            'code' => 200,
            'msg' => 'Success',
        ]);

    }
    public function teacherCourse(Request $request)
    {
        CourseTeacher::create($request);
        return response([
            'code' => 200,
            'msg' => 'Success',
        ]);
    }
}
