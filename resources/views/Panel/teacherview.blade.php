<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Admin | Dashboard | Teacher view</title>
    <!-- loader-->
    <link href="assets/css/pace.min.css" rel="stylesheet" />
    <script src="assets/js/pace.min.js"></script>
    <!--favicon-->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <!-- Vector CSS -->
    <link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- simplebar CSS-->
    <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <!-- Bootstrap core CSS-->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- animate CSS-->
    <link href="assets/css/animate.css" rel="stylesheet" type="text/css" />
    <!-- Icons CSS-->
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <!-- Sidebar CSS-->
    <link href="assets/css/sidebar-menu.css" rel="stylesheet" />
    <!-- Custom Style-->
    <link href="assets/css/app-style.css" rel="stylesheet" />
    <link href="assets/css/custom.css" rel="stylesheet" />

</head>

<body class="bg-theme bg-theme1">

    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->
        <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
            <div class="brand-logo">
                <a href="AdminDashboard.html">
                    <h5 class="logo-text">E-Tutor (Admin)</h5>
                </a>
            </div>
            <ul class="sidebar-menu do-nicescrol">
                <li class="sidebar-header">MAIN NAVIGATION</li>
                <li>
                    <a href="AdminDashboard.html">
                        <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="AddCourse.html">
                        <i class="zmdi zmdi-format-list-bulleted"></i> <span>Add Course</span>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i class="zmdi zmdi-lock"></i> <span>Logout</span>
                    </a>
                </li>


            </ul>

        </div>
        <!--End sidebar-wrapper-->

        <!--Start topbar header-->
        <header class="topbar-nav">
            <nav class="navbar navbar-expand fixed-top">
                <ul class="navbar-nav mr-auto align-items-center">
                    <li class="nav-item">
                        <a class="nav-link toggle-menu" href="javascript:void();">
                            <i class="icon-menu menu-icon"></i>
                        </a>
                    </li>
                </ul>

                <ul class="navbar-nav align-items-center right-nav-link">
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                            <span class="user-profile"><img src="https://via.placeholder.com/110x110" class="img-circle"
                                    alt="user avatar"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="dropdown-item user-details">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <div class="avatar"><img class="align-self-start mr-3"
                                                src="https://via.placeholder.com/110x110" alt="user avatar"></div>
                                        <div class="media-body">
                                            <h6 class="mt-2 user-title">Sabih</h6>
                                            <p class="user-subtitle">Admin</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>
        <!--End topbar header-->

        <div class="clearfix"></div>

        <div class="content-wrapper">
            <div class="container-fluid">

                <!--Start Dashboard Content-->

                <div class="row mt-3">
                    <div class="col-lg-4">
                      <div class="card profile-card-2">
                        <div class="card-img-block">
                          <img class="img-fluid" src="https://via.placeholder.com/800x500" alt="Card image cap">
                        </div>
                        <div class="card-body pt-5">
                          <img src="https://via.placeholder.com/110x110" alt="profile-image" class="profile">
                          <h5 class="card-title">Name</h5>
                        </div>
          
                        <div class="card-body border-top border-light">
                          <div class="media align-items-center">
                          </div>
                        </div>
                      </div>
          
                    </div>
          
                    <div class="col-lg-8">
                      <div class="card">
                        <div class="card-body">
                          <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified">
                            <li class="nav-item">
                              <a href="javascript:void();" data-target="#profile" data-toggle="pill" class="nav-link active"><i
                                  class="icon-user"></i> <span class="hidden-xs">Profile</span></a>
                            </li>
                          </ul>
                          <div class="tab-content p-3">
                            <div class="tab-pane active" id="profile">
                              <h5 class="mb-3">User Profile</h5>
                              <div class="row">
                                <div class="col-md-6">
                                  <h6>Name</h6>
                                  <p>
                                    Your name.
                                  </p>
                                </div>
                                <div class="col-md-6">
                                    <h6>Qualification</h6>
                                    <p>
                                      Your Qualifications
                                    </p>
                                  </div>
                                <div class="col-md-12">
                                  <div class="table-responsive">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <h6>Expertise</h6>
                                  <p>
                                    Your Expertise
                                  </p>
                                </div>
                                <div class="col-md-6">
                                    <h6>Bank Name</h6>
                                    <p>
                                      Your Bank Name
                                    </p>
                                  </div>
                                <div class="col-md-12">
                                  <div class="table-responsive">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <h6>Bank Account No.</h6>
                                  <p>
                                    Your Bank Account no.
                                  </p>
                                </div>
                                <div class="col-md-6">
                                    <h6>Assigned Course</h6>
                                    <p>
                                      Course
                                    </p>
                                  </div>
                                <div class="col-md-12">
                                  <div class="table-responsive">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <h6>Status</h6>
                                  <p>
                                    Pending/Approved.
                                  </p>
                                </div>
                                <div class="col-md-12">
                                  <div class="table-responsive">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-9">
                                    <input type="button" class="btn btn-primary" value="Block">
                                    <input type="button" class="btn btn-primary" value="UnblockBlock">
                                    <input type="button" class="btn btn-primary" value="Approved">
                                    <input type="button" class="btn btn-primary" value="Reject">
                                  </div>
                                  <div class="col-md-12">
                                    <div class="table-responsive">
                                    </div>
                                  </div>
                              </div>
                              <!--/row-->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
          
                  </div>          

                <!--End Dashboard Content-->

                <!--start overlay-->
                <div class="overlay toggle-menu"></div>
                <!--end overlay-->

            </div>
            <!-- End container-fluid-->

        </div>
        <!--End content-wrapper-->
        <!--Start Back To Top Button-->
        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
        <!--End Back To Top Button-->

        <!--start color switcher-->
        <div class="right-sidebar">
            <div class="switcher-icon">
                <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
            </div>
            <div class="right-sidebar-content">

                <p class="mb-0">Gaussion Texture</p>
                <hr>

                <ul class="switcher">
                    <li id="theme1"></li>
                    <li id="theme2"></li>
                    <li id="theme3"></li>
                    <li id="theme4"></li>
                    <li id="theme5"></li>
                    <li id="theme6"></li>
                </ul>
            </div>
        </div>
        <!--end color switcher-->

    </div>
    <!--End wrapper-->

    <!-- Bootstrap core JavaScript-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- simplebar js -->
    <script src="assets/plugins/simplebar/js/simplebar.js"></script>
    <!-- sidebar-menu js -->
    <script src="assets/js/sidebar-menu.js"></script>
    <!-- loader scripts -->
    <script src="assets/js/jquery.loading-indicator.js"></script>
    <!-- Custom scripts -->
    <script src="assets/js/app-script.js"></script>
    <!-- Chart js -->

    <script src="assets/plugins/Chart.js/Chart.min.js"></script>

    <!-- Index js -->
    <script src="assets/js/index.js"></script>


</body>

</html>