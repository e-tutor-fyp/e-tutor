<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>Teacher | Dashboard | Bank Details</title>
  <!-- loader-->
  <link href="assets/css/pace.min.css" rel="stylesheet" />
  <script src="assets/js/pace.min.js"></script>
  <!--favicon-->
  <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
  <!-- Vector CSS -->
  <link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
  <!-- simplebar CSS-->
  <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
  <!-- Bootstrap core CSS-->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <!-- animate CSS-->
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css" />
  <!-- Icons CSS-->
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
  <!-- Sidebar CSS-->
  <link href="assets/css/sidebar-menu.css" rel="stylesheet" />
  <!-- Custom Style-->
  <link href="assets/css/app-style.css" rel="stylesheet" />
  <link href="assets/css/custom.css" rel="stylesheet"/>

</head>

<body class="bg-theme bg-theme1">

  <!-- Start wrapper-->
  <div id="wrapper">

    <!--Start sidebar-wrapper-->
    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
      <div class="brand-logo">
        <a href="#">
          <h5 class="logo-text">E-Tutor</h5>
        </a>
      </div>
      <ul class="sidebar-menu do-nicescrol">
        <li class="sidebar-header">MAIN</li>
        <li>
          <a href="{{route('tdashboard')}}">
            <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

        <li>
          <a href="{{route('bdetail')}}">
            <i class="zmdi zmdi-grid"></i> <span>Bank Details</span>
          </a>
        </li>

        <li>
          <a href="{{route('tprofile')}}">
            <i class="zmdi zmdi-face"></i> <span>Profile</span>
          </a>
        </li>

        <li>
          <a href="#">
            <i class="zmdi zmdi-lock"></i> <span>Logout</span>
          </a>
        </li>
        
      </ul>
      <p class="tfo">2021 @ E-Tutor</p>

    </div>
    <!--End sidebar-wrapper-->

    <!--Start topbar header-->
    <header class="topbar-nav">
      <nav class="navbar navbar-expand fixed-top">
        <ul class="navbar-nav mr-auto align-items-center">
          <li class="nav-item">
            <a class="nav-link toggle-menu" href="javascript:void();">
              <i class="icon-menu menu-icon"></i>
            </a>
          </li>
        </ul>

        <ul class="navbar-nav align-items-center right-nav-link">
          <li class="nav-item">
            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
              <span class="user-profile"><img src="https://via.placeholder.com/110x110" class="img-circle"
                  alt="user avatar"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
              <li class="dropdown-item user-details">
                <a href="javaScript:void();">
                  <div class="media">
                    <div class="avatar"><img class="align-self-start mr-3" src="https://via.placeholder.com/110x110"
                        alt="user avatar"></div>
                    <div class="media-body">
                      <h6 class="mt-2 user-title">Name</h6>
                      <p class="user-subtitle">Tutor</p>
                    </div>
                  </div>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </header>
    <!--End topbar header-->

    <div class="clearfix"></div>

    <div class="content-wrapper">
      <div class="container-fluid">

        <!--Start Dashboard Content-->
        <section class="tbank">
        <div class="col-lg-6">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Bank Detail</h5>
                <div class="table-responsive">
                 <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Full Name</th>
                        <th scope="col">Bank Name</th>
                        <th scope="col">Account No.</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Full Name</td>
                        <td>Bank Name</td>
                        <td>Account No.</td>
                      </tr>
                    </tbody>
                  </table>
              </div>
              </div>
            </div>
        </section>

        <section>
          <div class="col-lg-6">
            <div class="card">
               <div class="card-body">
               <div class="card-title"></div>
               <hr>
                <form id="bankForm">
                {{ csrf_field() }}
               <div class="form-group">
                <label for="input-6">Name</label>
                <input type="text" name="name" class="form-control form-control-rounded" id="input-6" placeholder="Enter Your Full Name">
               </div>
               <div class="form-group">
                <label for="input-7">Bank Name</label>
                <input type="text" name="bankName" class="form-control form-control-rounded" id="input-7" placeholder="Enter Your Bank Name">
               </div>
               <div class="form-group">
                <label for="input-8">Bank Account No.</label>
                <input type="text" name="acoountNo" class="form-control form-control-rounded" id="input-8" placeholder="Enter Your Account No.">
               </div>
               <div class="form-group">
                <button type="submit" class="btn btn-light btn-round px-5"><i class="icon-lock"></i> Save</button>
              </div>
              </form>
             </div>
             </div>
          </div>
        </section>
        

        <!--End Dashboard Content-->

        <!--start overlay-->
        <div class="overlay toggle-menu"></div>
        <!--end overlay-->

      </div>
      <!-- End container-fluid-->

    </div>
    <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

    <!--start color switcher-->
    <div class="right-sidebar">
      <div class="switcher-icon">
        <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
      </div>
      <div class="right-sidebar-content">

        <p class="mb-0">Gaussion Texture</p>
        <hr>

        <ul class="switcher">
          <li id="theme1"></li>
          <li id="theme2"></li>
          <li id="theme3"></li>
          <li id="theme4"></li>
          <li id="theme5"></li>
          <li id="theme6"></li>
        </ul>

      </div>
    </div>
    <!--end color switcher-->

  </div>
  <!--End wrapper-->

  <!-- Bootstrap core JavaScript-->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- simplebar js -->
  <script src="assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="assets/js/sidebar-menu.js"></script>
  <!-- loader scripts -->
  <script src="assets/js/jquery.loading-indicator.js"></script>
  <!-- Custom scripts -->
  <script src="assets/js/app-script.js"></script>
  <!-- Chart js -->

  <script src="assets/plugins/Chart.js/Chart.min.js"></script>

  <!-- Index js -->
  <script src="assets/js/index.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#bankForm').on('submit', function(e) {
              e.preventDefault();
              // document.querySelector('.loader-wrapper').style.display = 'grid'
              var formData = new FormData(this);
              $.ajax({

                  type: "POST",
                  url: "#",
                  data: formData,
                  cache: false,
                  contentType: false,
                  processData: false,
                  success: function(response) {
                      if (response.code == 200) {
                          // printSuccessMsg(response.file);
                          console.log('Success');

                      } else {
                          // printErrorMsg(response.error);
                          console.log('Error')

                      }
                  }
              });
          });
      });

  </script>


</body>

</html>