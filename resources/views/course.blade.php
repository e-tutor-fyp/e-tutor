@extends('mainlayout')

@section('content')
  
<!-- page title -->
<section class="page-title-section overlay" data-background="images/backgrounds/page-title.jpg">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <ul class="list-inline custom-breadcrumb">
          <li class="list-inline-item"><a class="h2 text-primary font-secondary" href="courses.html">Our Courses</a></li>
          <li class="list-inline-item text-white h3 font-secondary "></li>
        </ul>
        <p class="text-lighten">Our courses offer a good compromise between the continuous assessment favoured by some universities and the emphasis placed on final exams by others.</p>
      </div>
    </div>
  </div>
</section>
<!-- courses -->
<section class="section">
  <div class="container">
    <!-- course list -->
<div class="row justify-content-center">
  <!-- course item -->
  @foreach ($course as $item)
  <div class="col-lg-4 col-sm-6 mb-5">
    <div class="card p-0 border-primary rounded-0 hover-shadow">
      <img class="card-img-top rounded-0" src="{{$item->image_link}}" alt="Photoshop">
      <div class="card-body">
        <ul class="list-inline mb-2">
          <li class="list-inline-item"><i class="ti-calendar mr-1 text-color"></i>2021</li>
        </ul>
        <a href="{{route('course-single', ['id' => $item->id])}}">
          <h4 class="card-title">{{$item->name}}</h4>
        </a>
        <p class="card-text mb-4"> {{$item->detail}}</p>
        <a href="{{route('course-single', ['id' => $item->id])}}"" class="btn btn-primary btn-sm">Learn More</a>
      </div>
      {{--  --}}
    </div>
  </div>
  @endforeach
  {{-- <!-- course item -->
  <div class="col-lg-4 col-sm-6 mb-5">
    <div class="card p-0 border-primary rounded-0 hover-shadow">
      <img class="card-img-top rounded-0" src="images/courses/Lightroom.png" alt="Adobe Lightroom">
      <div class="card-body">
        <ul class="list-inline mb-2">
          <li class="list-inline-item"><i class="ti-calendar mr-1 text-color"></i>2021</li>
        </ul>
        <a href="{{route('course-single')}}">
          <h4 class="card-title">Adobe Lightroom</h4>
        </a>
        <p class="card-text mb-4">Adobe Lightroom is a creative image organization and image manipulation software developed by Adobe Inc.
         as part of the Creative Cloud subscription family. It is supported on Windows, macOS, iOS, Android, and tvOS.</p>
        <a href="{{route('course-single')}}" class="btn btn-primary btn-sm">Learn More</a>
      </div>
    </div>
  </div>
  <!-- course item -->
  <div class="col-lg-4 col-sm-6 mb-5">
    <div class="card p-0 border-primary rounded-0 hover-shadow">
      <img class="card-img-top rounded-0" src="images/courses/inpixio.png" alt="inPixio">
      <div class="card-body">
        <ul class="list-inline mb-2">
          <li class="list-inline-item"><i class="ti-calendar mr-1 text-color"></i>2021</li>
        </ul>
        <a href="{{route('course-single')}}">
          <h4 class="card-title">inPixio</h4>
        </a>
        <p class="card-text mb-4">inPixio has specialized in digital photo editing software for many years. Our software helps photo enthusiasts to delete or cut out objects, create photomontages and optimize images – everything you need to retouch your photos! 
        Our photo editor software is developed for ease of use, unlimited creative flexibility and is highly intuitive, allowing for fast editing.</p>
        <a href="{{route('course-single')}}" class="btn btn-primary btn-sm">Learn More</a>
      </div>
    </div>
  </div>
  <!-- course item -->
  <div class="col-lg-4 col-sm-6 mb-5">
    <div class="card p-0 border-primary rounded-0 hover-shadow">
      <img class="card-img-top rounded-0" src="images/courses/Laravel.jpg" alt="Laravel">
      <div class="card-body">
        <ul class="list-inline mb-2">
          <li class="list-inline-item"><i class="ti-calendar mr-1 text-color"></i>2021</li>
        </ul>
        <a href="{{route('course-single')}}">
          <h4 class="card-title">Laravel</h4>
        </a>
        <p class="card-text mb-4"> Laravel is a free, open-source PHP web framework, created by Taylor Otwell and intended for 
        the development of web applications following the model–view–controller architectural pattern and based on Symfony.</p>
        <a href="{{route('course-single')}}" class="btn btn-primary btn-sm">Learn More</a>
      </div>
    </div>
  </div>
  <!-- course item -->
  <div class="col-lg-4 col-sm-6 mb-5">
    <div class="card p-0 border-primary rounded-0 hover-shadow">
      <img class="card-img-top rounded-0" src="images/courses/bootstrap1.jpg" alt="Bootstrap">
      <div class="card-body">
        <ul class="list-inline mb-2">
          <li class="list-inline-item"><i class="ti-calendar mr-1 text-color"></i>2021</li>
        </ul>
        <a href="{{route('course-single')}}">
          <h4 class="card-title">Bootstrap</h4>
        </a>
        <p class="card-text mb-4"> Bootstrap is a free and open-source CSS framework directed at responsive, mobile-first front-end web development. It contains CSS- and 
        JavaScript-based design templates for typography, forms, buttons, navigation, and other interfaces.</p>
        <a href="{{route('course-single')}}" class="btn btn-primary btn-sm">Learn More</a>
      </div>
    </div>
  </div>
  <!-- course item -->
  <div class="col-lg-4 col-sm-6 mb-5">
    <div class="card p-0 border-primary rounded-0 hover-shadow">
      <img class="card-img-top rounded-0" src="images/courses/wordpress1.jpeg" alt="Wordpress">
      <div class="card-body">
        <ul class="list-inline mb-2">
          <li class="list-inline-item"><i class="ti-calendar mr-1 text-color"></i>2021</li>
        </ul>
        <a href="{{route('course-single')}}">
          <h4 class="card-title">Wordpress</h4>
        </a>
        <p class="card-text mb-4"> WordPress is a free and open-source content management system written in PHP and paired with a MySQL or MariaDB database. Features 
        include a plugin architecture and a template system, referred to within WordPress as Themes.</p>
        <a href="{{route('course-single')}}" class="btn btn-primary btn-sm">Learn More</a>
      </div>
    </div>
  </div>
  <!-- course item -->
  <div class="col-lg-4 col-sm-6 mb-5">
    <div class="card p-0 border-primary rounded-0 hover-shadow">
      <img class="card-img-top rounded-0" src="images/courses/javascript.png" alt="JavaScript">
      <div class="card-body">
        <ul class="list-inline mb-2">
          <li class="list-inline-item"><i class="ti-calendar mr-1 text-color"></i>2021</li>
        </ul>
        <a href="{{route('course-single')}}">
          <h4 class="card-title">Javascript</h4>
        </a>
        <p class="card-text mb-4"> JavaScript, often abbreviated as JS, is a programming language that conforms to the ECMAScript specification. JavaScript is high-level, often just-in-time compiled, and multi-paradigm. 
        It has curly-bracket syntax, dynamic typing, prototype-based object-orientation, and first-class functions.</p>
        <a href="{{route('course-single')}}" class="btn btn-primary btn-sm">Learn More</a>
      </div>
    </div>
  </div>
  <!-- course item -->
<div class="col-lg-4 col-sm-6 mb-5">
    <div class="card p-0 border-primary rounded-0 hover-shadow">
      <img class="card-img-top rounded-0" src="images/courses/Freelancing.jpg" alt="Freelacing">
      <div class="card-body">
        <ul class="list-inline mb-2">
          <li class="list-inline-item"><i class="ti-calendar mr-1 text-color"></i>2021</li>
          </ul>
        <a href="{{route('course-single')}}">
          <h4 class="card-title">Freelancing</h4>
        </a>
        <p class="card-text mb-4"> Freelancer is an Australian freelance marketplace website, which allows potential employers to post jobs that freelancers can then bid to complete. Founded in 2009, its headquarters is located in Sydney, 
        Australia, though it also has offices in Vancouver, London, Buenos Aires, Manila, and Jakarta.</p>
        <a href="{{route('course-single')}}" class="btn btn-primary btn-sm">Learn More</a>
      </div>
    </div>
  </div>
  <!-- course item -->
<div class="col-lg-4 col-sm-6 mb-5">
    <div class="card p-0 border-primary rounded-0 hover-shadow">
      <img class="card-img-top rounded-0" src="images/courses/Upwork.jpg" alt="Upwork">
      <div class="card-body">
        <ul class="list-inline mb-2">
          <li class="list-inline-item"><i class="ti-calendar mr-1 text-color"></i>2021</li>
          </ul>
        <a href="{{route('course-single')}}">
          <h4 class="card-title">Upwork</h4>
        </a>
        <p class="card-text mb-4"> Upwork, formerly Elance-oDesk, is an American freelancing platform where enterprises and individuals connect in order to conduct business. In 2015, the 
        Elance-oDesk merger was rebranded as Upwork and the company's full name is now Upwork Global Inc.</p>
        <a href="{{route('course-single')}}" class="btn btn-primary btn-sm">Learn More</a>
      </div>
    </div>
  </div>
</div>
<!-- /course list --> --}}
    <!-- course list -->
<div class="row justify-content-center">
<!-- /course list -->
  </div>
</section>
<!-- /courses -->




@endsection
