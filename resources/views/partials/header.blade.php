  <!-- preloader start -->
  <div class="preloader">
      <img src="{{URL('images/preloader.gif')}}" alt="preloader">
  </div>
  <!-- preloader end -->

  <!-- header -->
  <header class="fixed-top header">
      <!-- top header -->
      <div class="top-header py-2 bg-white">
          <div class="container">
              <div class="row no-gutters">
                  <div class="col-lg-4 text-center text-lg-left">
                      <a class="text-color mr-3" href="callto:+443003030266"><strong>CALL</strong> +92 xxx xxx xxxx</a>
                      <ul class="list-inline d-inline">
                          <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i
                                      class="ti-facebook"></i></a></li>
                          <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i
                                      class="ti-twitter-alt"></i></a></li>
                          <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i
                                      class="ti-linkedin"></i></a></li>
                          <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i
                                      class="ti-instagram"></i></a></li>
                      </ul>
                  </div>
                  <div class="col-lg-8 text-center text-lg-right">
                      <ul class="list-inline">
                          <li class="list-inline-item"><a
                                  class="text-uppercase text-color p-sm-2 py-2 px-0 d-inline-block"
                                  href="{{ route('notice') }}">notice</a></li>
                          <li class="list-inline-item"><a
                                  class="text-uppercase text-color p-sm-2 py-2 px-0 d-inline-block" href="#"
                                  data-toggle="modal" data-target="#loginModal">login</a></li>
                          <li class="list-inline-item"><a
                                  class="text-uppercase text-color p-sm-2 py-2 px-0 d-inline-block" href="#"
                                  data-toggle="modal" data-target="#signupModal">register</a></li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
      <!-- navbar -->
      <div class="navigation w-100">
          <div class="container">
              <nav class="navbar navbar-expand-lg navbar-dark p-0">
                  <a class="navbar-brand" href="{{ route('home') }}"><img src="{{URL('images/favicon.png')}}" alt="logo"></a>
                  <h2 class="logo-text">E-<span>tutor</span></h2>
                  <button class="navbar-toggler rounded-0" type="button" data-toggle="collapse"
                      data-target="#navigation" aria-controls="navigation" aria-expanded="false"
                      aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>

                  <div class="collapse navbar-collapse" id="navigation">
                      <ul class="navbar-nav ml-auto text-center">
                          <li class="nav-item ">
                              <a class="nav-link" href="{{ route('home') }}">Home</a>
                          </li>
                          <li class="nav-item @@about">
                              <a class="nav-link" href="{{ route('about') }}">About</a>
                          </li>
                          <li class="nav-item @@courses">
                              <a class="nav-link" href="{{ route('course') }}">COURSES</a>
                          </li>
                          <li class="nav-item @@events">
                              <a class="nav-link" href="{{ route('teachers') }}">TEACHERS</a>
                          </li>
                          <!-- <li class="nav-item dropdown view">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Pages
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="teacher.html">Teacher</a>
                <a class="dropdown-item" href="teacher-single.html">Teacher Single</a>
                {{-- <a class="dropdown-item" href="{{ route('notice') }}">Notice</a> --}}
                {{-- <a class="dropdown-item" href="{{ route('notice-single') }}">Notice Details</a> --}}
                {{-- <a class="dropdown-item" href="{{ route('research') }}">Research</a> --}}
                {{-- <a class="dropdown-item" href="{{ route('scolarship') }}">Scholarship</a> --}}
                {{-- <a class="dropdown-item" href="{{ route('course-single') }}">Course Details</a> --}}
              </div>
            </li> -->
                          <li class="nav-item @@contact">
                              <a class="nav-link" href="{{ route('contact') }}">CONTACT</a>
                          </li>
                      </ul>
                  </div>
              </nav>
          </div>
      </div>
  </header>
  <!-- /header -->
  <!-- Modal -->
  <div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content rounded-0 border-0 p-4">
              <div class="modal-header border-0">
                  <h3>Register</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="login">
                      <form class="row" id="signupForm">
                          {{ csrf_field() }}
                          <div class="col-12">
                              <input type="text" class="form-control mb-3" id="name" name="name" placeholder="Name">
                          </div>
                          <div class="col-12">
                              <input type="email" class="form-control mb-3" id="email" name="email" placeholder="Email">
                          </div>
                          <div class="col-12">
                              <input type="password" class="form-control mb-3" id="password" name="password"
                                  placeholder="Password">
                          </div>
                          <div class="col-12">
                              <select class="form-control" id="role" name="role">
                                  <option value="student">Student</option>
                                  <option value="teacher">Teacher</option>
                              </select>
                          </div>
                          <div class="col-12">
                              <button type="submit" class="btn btn-primary">SIGN UP</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content rounded-0 border-0 p-4">
              <div class="modal-header border-0">
                  <h3>Login</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <form class="row" id="LoginForm" method="post" action="/login" enctype="multipart/form-data">
                      {{ csrf_field() }}

                      <div class="col-12">
                          <input type="email" class="form-control mb-3" id="Email" name="Email" placeholder="Email">
                      </div>
                      <div class="col-12">
                          <input type="password" class="form-control mb-3" id="Password" name="Password"
                              placeholder="Password">
                      </div>
                      <div class="col-12">
                          <select class="form-control" id="sel1" id="role" name="role">
                              <option value="student">Student</option>
                              <option value="teacher">Teacher</option>
                              <option value="admin">Admin</option>
                          </select>
                      </div>
                      <div class="col-12">
                          <button type="submit" class="btn btn-primary  mt-5"> LOGIN</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
  <script type="text/javascript">
      $(document).ready(function() {
          $('#signupForm').on('submit', function(e) {
              e.preventDefault();
              // document.querySelector('.loader-wrapper').style.display = 'grid'
              var formData = new FormData(this);
              $.ajax({

                  type: "POST",
                  url: "{{ route('register') }}",
                  data: formData,
                  cache: false,
                  contentType: false,
                  processData: false,
                  success: function(response) {
                      if (response.code == 200) {
                          // printSuccessMsg(response.file);
                          console.log('Success');

                      } else {
                          // printErrorMsg(response.error);
                          console.log('Error')

                      }
                  }
              });
          });
      });

  </script>
   {{-- <script type="text/javascript">
      $(document).ready(function() {
          $('#LoginForm').on('submit', function(e) {
              e.preventDefault();
              // document.querySelector('.loader-wrapper').style.display = 'grid'
              var formData = new FormData(this);
              $.ajax({

                  type: "POST",
                  url: "{{ route('login') }}",
                  data: formData,
                  cache: false,
                  contentType: false,
                  processData: false,
                  success: function(response) {
                      if (response.code == 200) {
                          // printSuccessMsg(response.file);
                          console.log('Success');

                     } else {
                          // printErrorMsg(response.error);
                          console.log('Error')

                      }
                  }
              });
          });
      });

  </script> --}}
