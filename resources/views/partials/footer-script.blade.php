<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script src="{{URL('plugins/jQuery/jquery.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{URL('plugins/bootstrap/bootstrap.min.js')}}"></script>
<!-- slick slider -->
<script src="{{URL('plugins/slick/slick.min.js')}}"></script>
<!-- aos -->
<script src="{{URL('plugins/aos/aos.js')}}"></script>
<!-- venobox popup -->
<script src="{{URL('plugins/venobox/venobox.min.js')}}"></script>
<!-- filter -->
<script src="{{URL('plugins/filterizr/jquery.filterizr.min.js')}}"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnL1KV5c4Xd8QSjZ4gKe6bkInyO4dQVCw"></script>
<script src="{{URL('plugins/google-map/gmap.js')}}"></script>

<!-- Main Script -->
<script src="{{URL('js/script.js')}}"></script>
