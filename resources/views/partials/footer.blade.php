<!-- footer -->
<footer>
  <!-- newsletter -->
  <div class="newsletter">
    <div class="container">
      <div class="row">
        <div class="col-md-9 ml-auto bg-primary py-5 newsletter-block">
          <h3 class="text-white">Subscribe Now</h3>
          <form id="subscribeForm">
          {{ csrf_field() }}
            <div class="input-wrapper">
              <input type="email" class="form-control border-0" id="newsletter" name="newsletter" placeholder="Enter Your Email...">
              <button type="submit" value="send" class="btn btn-primary">Join</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- footer content -->
  <div class="footer bg-footer section border-bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-sm-8 mb-5 mb-lg-0">
          <!-- logo -->
          <a class="logo-footer" href="{{route('home')}}"><img  src="images/favicon.png" alt="logo"><h2 class="logo-text">E-<span>tutor</span></h2></a>
          <ul class="list-unstyled">
            <li class="mb-2">Superior Gold Campus, Riwand Road Lahore.</li>
            <li class="mb-2">+92 xxx xxx xxxx</li>
            <li class="mb-2">+92 xxx xxx xxxx</li>
            <li class="mb-2">Info@Etutor.com</li>
          </ul>
        </div>
        <!-- company -->
        <div class="col-lg-2 col-md-3 col-sm-4 col-6 mb-5 mb-md-0">
          <h4 class="text-white mb-5">COMPANY</h4>
          <ul class="list-unstyled">
            <li class="mb-3"><a class="text-color" href="{{route('about')}}">About Us</a></li>
            <li class="mb-3"><a class="text-color" href="{{route('teachers')}}">Our Teacher</a></li>
            <li class="mb-3"><a class="text-color" href="{{route('contact')}}">Contact</a></li>
            <li class="mb-3"><a class="text-color" href="{{route('notice')}}">Notice</a></li>
          </ul>
        </div>
        <!-- links -->
        <div class="col-lg-2 col-md-3 col-sm-4 col-6 mb-5 mb-md-0">
          <h4 class="text-white mb-5">LINKS</h4>
          <ul class="list-unstyled">
            <li class="mb-3"><a class="text-color" href="{{route('course')}}">Courses</a></li>
            <li class="mb-3"><a class="text-color" href="#">login</a></li>
            <li class="mb-3"><a class="text-color" href="#">Register</a></li>
          </ul>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-4 col-6 mb-5 mb-md-0">
          <h4 class="text-white mb-5">RECOMMEND</h4>
          <ul class="list-unstyled">
            {{-- <li class="mb-3"><a class="text-color" href="{{route('course-single')}}">WordPress</a></li> --}}
            {{-- <li class="mb-3"><a class="text-color" href="{{route('course-single')}}">Bootstrap</a></li> --}}
            {{-- <li class="mb-3"><a class="text-color" href="{{route('course-single')}}">Laravel</a></li> --}}
            {{-- <li class="mb-3"><a class="text-color" href="{{route('course-single')}}">Photoshop</a></li> --}}
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- copyright -->
  <div class="copyright py-4 bg-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 text-sm-left text-center">
          <p class="mb-0">Copyright
            <script>
              var CurrentYear = new Date().getFullYear()
              document.write(CurrentYear)
            </script>
            ©</p>
        </div>
        <div class="col-sm-5 text-sm-right text-center">
          <ul class="list-inline">
            <li class="list-inline-item"><a class="d-inline-block p-2" href="#"><i class="ti-facebook text-primary"></i></a></li>
            <li class="list-inline-item"><a class="d-inline-block p-2" href="#"><i class="ti-twitter-alt text-primary"></i></a></li>
            <li class="list-inline-item"><a class="d-inline-block p-2" href="#"><i class="ti-linkedin text-primary"></i></a></li>
            <li class="list-inline-item"><a class="d-inline-block p-2" href="#"><i class="ti-instagram text-primary"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- /footer -->

<script type="text/javascript">
      $(document).ready(function() {
          $('#subscribeForm').on('submit', function(e) {
              e.preventDefault();
              // document.querySelector('.loader-wrapper').style.display = 'grid'
              var formData = new FormData(this);
              $.ajax({

                  type: "POST",
                  url: "#",
                  data: formData,
                  cache: false,
                  contentType: false,
                  processData: false,
                  success: function(response) {
                      if (response.code == 200) {
                          // printSuccessMsg(response.file);
                          console.log('Success');

                      } else {
                          // printErrorMsg(response.error);
                          console.log('Error')

                      }
                  }
              });
          });
      });

  </script>