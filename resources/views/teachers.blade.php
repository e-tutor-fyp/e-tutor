@extends('mainlayout')

@section('content')

<!-- page title -->
<section class="page-title-section overlay" data-background="images/backgrounds/page-title.jpg">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <ul class="list-inline custom-breadcrumb">
          <li class="list-inline-item"><a class="h2 text-primary font-secondary" href="@@page-link">Teachers</a></li>
          <li class="list-inline-item text-white h3 font-secondary @@nasted"></li>
        </ul>
        <p class="text-lighten">We provide that teachers who expert in their profession</p>
      </div>
    </div>
  </div>
</section>
<!-- /page title -->

<!-- courses -->
<section class="section">
  <div class="container">
    <div class="row">
      <!-- Teacher -->
      <div class="col-lg-4 col-sm-6 mb-5">
        <div class="card border-0 rounded-0 hover-shadow">
          <div class="card-img position-relative">
            <img class="card-img-top rounded-0" src="images/events/event-1.jpg" alt="Umar">
          </div>
          <div class="card-body">
            <p>Laravel, Programming</p>
              <h4 class="card-title">Muhammad Umar</h4>
          </div>
        </div>
      </div>
      <!-- Teacher -->
      <div class="col-lg-4 col-sm-6 mb-5">
        <div class="card border-0 rounded-0 hover-shadow">
          <div class="card-img position-relative">
            <img class="card-img-top rounded-0" src="images/events/event-2.jpg" alt="Sabih">
          </div>
          <div class="card-body">
            <p>Bootstrap and JavaScript, Programming</p>
              <h4 class="card-title">Sabih Tahir</h4>
          </div>
        </div>
      </div>
      <!-- Teacher -->
      <div class="col-lg-4 col-sm-6 mb-5">
        <div class="card border-0 rounded-0 hover-shadow">
          <div class="card-img position-relative">
            <img class="card-img-top rounded-0" src="images/events/event-3.jpg" alt="Bilal">
          </div>
          <div class="card-body">
            <p>Photoshop, Photo Editer</p>
              <h4 class="card-title">Bilal Ghafoor</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /courses -->


@endsection
