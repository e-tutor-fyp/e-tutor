<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'viewsController@index')->name('home');
Route::get('/about-us', 'viewsController@about')->name('about');
Route::get('/contact', 'viewsController@contact')->name('contact');
Route::get('/course', 'viewsController@course')->name('course');
Route::get('/course-single/{id}', 'viewsController@course_single')->name('course-single');
Route::get('/teachers', 'viewsController@teachers')->name('teachers');
Route::get('/teacher-single', 'viewsController@teacher_single')->name('teacher-single');
Route::get('/notice', 'viewsController@notice')->name('notice');
Route::get('/notice-single', 'viewsController@notice_single')->name('notice-single');
Route::get('/research', 'viewsController@research')->name('research');
Route::get('/scolarship', 'viewsController@scolarship')->name('scolarship');

//Panel

Route::get('/studentDashboard', 'viewsController@studentDashboard')->name('Sdashboard');
Route::get('/paymentDetails', 'viewsController@paymentDetails')->name('Payment');
Route::get('/profile', 'viewsController@profile')->name('SProfile');
Route::get('/teacherdashboard', 'viewsController@teacherdashboard')->name('tdashboard');
Route::get('/BankDetails', 'viewsController@BankDetails')->name('bdetail');
Route::get('/tprofile', 'viewsController@tprofile')->name('tprofile');
Route::get('/admindashboard', 'viewsController@admindashboard')->name('adashboard');
Route::get('/addcourse', 'viewsController@addcourse')->name('acourse');

//Authentication:

Route::post('/register', 'AuthController@Register')->name('register');
Route::post('/login', 'AuthController@login')->name('login');
Route::post('/logout', 'AuthController@logout')->name('logout');
